    var yung_global_var = {
        _global_ajaxTimeout : null
    };
    var _base;
    function _packageCheck(packages) {
        if (typeof packages == "string") {
            packages = packages.split(".");
        }
        _base = window;
        if (packages != null && packages.length > 0) {
            for (var i = 0; i < packages.length; i++) {
                if (_base[packages[i]] == null) {
                    _base[packages[i]] = {};
                }
                _base = _base[packages[i]];
            }
        }
        return _base;
    }
    
    /* Simple JavaScript Inheritance
     * By John Resig http://ejohn.org/
     * MIT Licensed.
     */
    // Inspired by base2 and Prototype
    (function(){
      var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
     
      // The base AbstractClass implementation (does nothing)
      this.AbstractClass = function(){};
     
      // Create a new AbstractClass that inherits from this class
      AbstractClass.extend = function(prop) {
        var _super = this.prototype;
       
        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;
        var cls = null;
        var pack = null;
        
        // Copy the properties over onto the new prototype
        for (var name in prop) {
          if (name === 'classProp') {
              try {
                  Object.freeze(prop[name]);
              } catch (err) {
                  // for IE quirk mode
                  }
              var className = prop[name].name;
              var packages = className.split(".");
              if (packages.length > 1) {
                cls = packages[packages.length - 1];
                packages.splice(packages.length - 1, 1);
                pack = _packageCheck(packages);
              } else {
                cls = null;
                pack = null;
              }
          }              
          // Check if we're overwriting an existing function
          prototype[name] = typeof prop[name] == "function" &&
            typeof _super[name] == "function" && fnTest.test(prop[name]) ?
            (function(name, fn){
              return function() {
                var tmp = this._super;
               
                // Add a new ._super() method that is the same method
                // but on the super-class
                this._super = _super[name];
               
                // The method only need to be bound temporarily, so we
                // remove it when we're done executing
                var ret = fn.apply(this, arguments);        
                this._super = tmp;
               
                return ret;
              };
            })(name, prop[name]) :
            prop[name];
        }
       
        // The dummy class constructor
        function AbstractClass() {
          // All construction is actually done in the init method
          if ( !initializing && this.init )
            this.init.apply(this, arguments);
        }
       
        // Populate our constructed prototype object
        AbstractClass.prototype = prototype;
       
        // Enforce the constructor to be what we expect
        AbstractClass.prototype.constructor = $Class;
     
        // And make this class extendable
        AbstractClass.extend = arguments.callee;
        
        if (pack != null && cls != null) {
            pack[cls] = AbstractClass;
        }
        return AbstractClass;
      };
    })();
    /**
     * Basic Class
     * Yung Long Li
     */
    var $Class = AbstractClass.extend({
    	// for reflection
    	classProp : { name : "$Class"},
        debugPrint: function(){
            var result = "object: " + this.classProp.name + "\n";
            for (var key in this) {
                if (!(typeof(this[key]) === typeof(Function) || key == "_super")){
                    var value = this[key];
                    //if(typeof(value) === "undefined"){
                        result = result + "    " + key + ": " + value + "\n";
                    //}
                }
            }
            alert(result);
        },
        hashCode : function () {
            var json = JSON.stringify(this);
            if (this.classProp != null && this.classProp.name != null) {
                json = this.classProp.name + json;
            }
            return MD5Util.calc(json);
        },
        toString : function () {
            if (this.classProp != null && this.classProp.name != null) {
                return this.classProp.name + JSON.stringify(this);
            } else {
                return JSON.stringify(this);
            }
        }
    });
    
    window["$Y"] = function () {};
    window["$Y"]["register"] = function (className, type) {
        var clsArray = className.split(".");
        if (clsArray.length == 1) {
            return;
        }
        var idx = className.lastIndexOf(".");
        var packageName = className.substring(0, idx);
        var pack = _packageCheck(packageName);
        var clsName = className.substring(idx + 1, className.length);
        if (type != null) {
            if (pack[clsName] == null) {
                if (typeof type == "function") {
                    pack[clsName] = type;
                    return type;
                } else if (typeof type == "object") {
                    pack[clsName] = type;
                    return type;
                } else {
                    throw "argument type is not function";
                }
            } else {
                throw className + " is already registered";
            }
        }
    }
    
    window["$Y"]["import"] = function (className) {
        if (typeof className != "string") {
            return;
        }
        var clsArray = className.split(".");
        if (clsArray.length == 1) {
            return;
        }
        var idx = className.lastIndexOf(".");
        var packageName = className.substring(0, idx);
        var pack = _packageCheck(packageName);
        var clsName = className.substring(idx + 1, className.length);
        if (clsName == '*') {
            for (var key in pack) {
                if (typeof pack[key] == 'function') {
                    $Y[key] = pack[key];
                } else if (typeof pack[key] == 'object') {
                    // for static import
                    $Y[key] = pack[key];
                }
            }
        } else {
            if (typeof pack[clsName] == 'function') {
                $Y[clsName] = pack[clsName];
            } else if (typeof pack[clsName] == 'object') {
                // for static import
                $Y[clsName] = pack[clsName];
            } else if (pack[clsName] == null) {
                $Y.register(className);
                $Y[clsName] = pack[clsName];
            }
        }
    }
    window["$Y"]["def"] = window["$Y"]["import"];
    
    /**
     * show AJAX loading 
     */
    function ajaxLoadingShow (){
    	$('#loadingIMG').show();
    	$('#loadingIMG').focus();
    }
    
    /**
     * hide AJAX loading 
     */
    function ajaxLoadingHide (){
    	$('#loadingIMG').hide();
    }
    
    var MD5Util = $Class.extend({
        classProp : { name : "MD5Util"},
        hex_chr: "0123456789abcdef",
        init : function(){
            return this;
        },
        rhex: function (num){
            var str = "";
            for(var j = 0; j <= 3; j++) {
                str += this.hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) + this.hex_chr.charAt((num >> (j * 8)) & 0x0F);
            }
            return str;
        },
        init: function(name){
            this.name = name;
            return this;
        },
        str2blks_MD5 : function(str){
            var nblk = ((str.length + 8) >> 6) + 1;
            var blks = new Array(nblk * 16);
            for(i = 0; i < nblk * 16; i++) {
                blks[i] = 0;
            }
            for(i = 0; i < str.length; i++) {
                blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
            }
            blks[i >> 2] |= 0x80 << ((i % 4) * 8);
            blks[nblk * 16 - 2] = str.length * 8;
            return blks;
        },
        add : function(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        },
        rol : function(num, cnt) {
            return (num << cnt) | (num >>> (32 - cnt));
        },
        cmn : function (q, a, b, x, s, t) {
            return this.add(this.rol(this.add(this.add(a, q), this.add(x, t)), s), b);
        },
        ff : function (a, b, c, d, x, s, t) {
            return this.cmn((b & c) | ((~b) & d), a, b, x, s, t);
        },
        gg : function (a, b, c, d, x, s, t) {
            return this.cmn((b & d) | (c & (~d)), a, b, x, s, t);
        },
        hh : function (a, b, c, d, x, s, t) {
            return this.cmn(b ^ c ^ d, a, b, x, s, t);
        },
        ii : function (a, b, c, d, x, s, t) {
            return this.cmn(c ^ (b | (~d)), a, b, x, s, t);
        },
        calcMD5 : function(str) {
            x = this.str2blks_MD5(str);
            a =  1732584193;
            b = -271733879;
            c = -1732584194;
            d =  271733878;
            for(i = 0; i < x.length; i += 16) {
                olda = a;
                oldb = b;
                oldc = c;
                oldd = d;
                a = this.ff(a, b, c, d, x[i+ 0], 7 , -680876936);
                d = this.ff(d, a, b, c, x[i+ 1], 12, -389564586);
                c = this.ff(c, d, a, b, x[i+ 2], 17,  606105819);
                b = this.ff(b, c, d, a, x[i+ 3], 22, -1044525330);
                a = this.ff(a, b, c, d, x[i+ 4], 7 , -176418897);
                d = this.ff(d, a, b, c, x[i+ 5], 12,  1200080426);
                c = this.ff(c, d, a, b, x[i+ 6], 17, -1473231341);
                b = this.ff(b, c, d, a, x[i+ 7], 22, -45705983);
                a = this.ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
                d = this.ff(d, a, b, c, x[i+ 9], 12, -1958414417);
                c = this.ff(c, d, a, b, x[i+10], 17, -42063);
                b = this.ff(b, c, d, a, x[i+11], 22, -1990404162);
                a = this.ff(a, b, c, d, x[i+12], 7 ,  1804603682);
                d = this.ff(d, a, b, c, x[i+13], 12, -40341101);
                c = this.ff(c, d, a, b, x[i+14], 17, -1502002290);
                b = this.ff(b, c, d, a, x[i+15], 22,  1236535329);
                a = this.gg(a, b, c, d, x[i+ 1], 5 , -165796510);
                d = this.gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
                c = this.gg(c, d, a, b, x[i+11], 14,  643717713);
                b = this.gg(b, c, d, a, x[i+ 0], 20, -373897302);
                a = this.gg(a, b, c, d, x[i+ 5], 5 , -701558691);
                d = this.gg(d, a, b, c, x[i+10], 9 ,  38016083);
                c = this.gg(c, d, a, b, x[i+15], 14, -660478335);
                b = this.gg(b, c, d, a, x[i+ 4], 20, -405537848);
                a = this.gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
                d = this.gg(d, a, b, c, x[i+14], 9 , -1019803690);
                c = this.gg(c, d, a, b, x[i+ 3], 14, -187363961);
                b = this.gg(b, c, d, a, x[i+ 8], 20,  1163531501);
                a = this.gg(a, b, c, d, x[i+13], 5 , -1444681467);
                d = this.gg(d, a, b, c, x[i+ 2], 9 , -51403784);
                c = this.gg(c, d, a, b, x[i+ 7], 14,  1735328473);
                b = this.gg(b, c, d, a, x[i+12], 20, -1926607734);                
                a = this.hh(a, b, c, d, x[i+ 5], 4 , -378558);
                d = this.hh(d, a, b, c, x[i+ 8], 11, -2022574463);
                c = this.hh(c, d, a, b, x[i+11], 16,  1839030562);
                b = this.hh(b, c, d, a, x[i+14], 23, -35309556);
                a = this.hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
                d = this.hh(d, a, b, c, x[i+ 4], 11,  1272893353);
                c = this.hh(c, d, a, b, x[i+ 7], 16, -155497632);
                b = this.hh(b, c, d, a, x[i+10], 23, -1094730640);
                a = this.hh(a, b, c, d, x[i+13], 4 ,  681279174);
                d = this.hh(d, a, b, c, x[i+ 0], 11, -358537222);
                c = this.hh(c, d, a, b, x[i+ 3], 16, -722521979);
                b = this.hh(b, c, d, a, x[i+ 6], 23,  76029189);
                a = this.hh(a, b, c, d, x[i+ 9], 4 , -640364487);
                d = this.hh(d, a, b, c, x[i+12], 11, -421815835);
                c = this.hh(c, d, a, b, x[i+15], 16,  530742520);
                b = this.hh(b, c, d, a, x[i+ 2], 23, -995338651);
                a = this.ii(a, b, c, d, x[i+ 0], 6 , -198630844);
                d = this.ii(d, a, b, c, x[i+ 7], 10,  1126891415);
                c = this.ii(c, d, a, b, x[i+14], 15, -1416354905);
                b = this.ii(b, c, d, a, x[i+ 5], 21, -57434055);
                a = this.ii(a, b, c, d, x[i+12], 6 ,  1700485571);
                d = this.ii(d, a, b, c, x[i+ 3], 10, -1894986606);
                c = this.ii(c, d, a, b, x[i+10], 15, -1051523);
                b = this.ii(b, c, d, a, x[i+ 1], 21, -2054922799);
                a = this.ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
                d = this.ii(d, a, b, c, x[i+15], 10, -30611744);
                c = this.ii(c, d, a, b, x[i+ 6], 15, -1560198380);
                b = this.ii(b, c, d, a, x[i+13], 21,  1309151649);
                a = this.ii(a, b, c, d, x[i+ 4], 6 , -145523070);
                d = this.ii(d, a, b, c, x[i+11], 10, -1120210379);
                c = this.ii(c, d, a, b, x[i+ 2], 15,  718787259);
                b = this.ii(b, c, d, a, x[i+ 9], 21, -343485551);
                a = this.add(a, olda);
                b = this.add(b, oldb);
                c = this.add(c, oldc);
                d = this.add(d, oldd);
            }
            return this.rhex(a) + this.rhex(b) + this.rhex(c) + this.rhex(d);
        }
    });
    var _md5Util = new MD5Util();
    MD5Util.calc = function (str) {
        return _md5Util.calcMD5(str);
    }
    