/*
 * Construcs a new toolbar for the spreadjs window
 *
 * author: Yung Long Li
 */
    var com_yung_util_Toolbar = $Class.extend({
        classProp : { name : "com.yung.util.Toolbar" },
        container : null,
        btnCSS : null,
        color : null,
        init: function(containerDiv, btnCSS, color){
            if (containerDiv == null) {
                throw 'please specify container div id';
            }
            var divEle = document.getElementById(containerDiv);
            if (divEle == null) {
                throw 'container div not exists!';
            }
            if (color == null) {
                color = "rgb(234, 234, 234)";
            }
            this.color = color;
            jQuery("#" + containerDiv).css("width", "100%");
            jQuery("#" + containerDiv).css("height", "24px");
            jQuery("#" + containerDiv).css("padding-top", "5px");
            jQuery("#" + containerDiv).css("padding-bottom", "5px");
            jQuery("#" + containerDiv).css("margin", "1px");
            jQuery("#" + containerDiv).css("background-color", color);
            this.container = divEle;
            this.btnCSS = document.getElementById(btnCSS);
            if (this.btnCSS == null) {
                var css = document.createElement("style");
                css.type = "text/css";
                document.body.appendChild(css);
                this.btnCSS = css;
            }
            return this;
        },
        initElement : function(id, elt, label, tooltip) {
            elt.setAttribute('id', id);
            if (label != null && label != '') {
                elt.innerHTML = label;
                elt.setAttribute('style', 'height: 19px;');
            } else {
                elt.setAttribute('style', 'width: 20px;');
            }
            // Adds tooltip
            if (tooltip != null) {
                elt.setAttribute('title', tooltip);
            }
        },
        addSeparator : function() {
            var elt = document.createElement('span');
            jQuery(elt).css("float", "left");
            jQuery(elt).css("text-decoration", "none");
            jQuery(elt).css("margin", "0px");
            jQuery(elt).css("font-family", "Arial");
            jQuery(elt).css("font-size", "17px");
            jQuery(elt).css("height", "19px");
            jQuery(elt).css("padding-left", "5px");
            jQuery(elt).css("padding-right", "5px");
            jQuery(elt).css("color", "rgb(33, 115, 70)");
            elt.innerHTML = "&vert;";
            this.container.appendChild(elt);
            return elt;
        },
        addItems : function(items) {
            for ( var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemImg = item.img;
                var itemLabel = item.label;
                var itemTooltip = item.tooltip;
                var itemFunct = item.funct;
                if (itemImg == '-' || itemLabel == '-') {
                    this.addSeparator();
                } else {
                    this.addItem('toolbtn-' + i, itemImg, itemLabel, itemTooltip);
                }
            }
            this.initFunction(items);
        },
        initFunction : function(items) {
            for ( var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemFunct = item.funct;
                if (itemFunct != null) {
                    this.addClickHandler('toolbtn-' + i, itemFunct);
                }
            }
        },
        addItem : function(id, itemImg, label, tooltip) {
            var elt = this.addButton(id, itemImg, label, tooltip);
            this.createCSS(itemImg, tooltip);
            return elt;
        },
        addButton : function(id, itemImg, label, tooltip) {
            var elt = this.createButton(itemImg, tooltip);
            this.initElement(id, elt, label, tooltip);
            this.container.appendChild(elt);
            return elt;
        },
        addClickHandler : function(id, funct) {
            if (funct != null) {
                var ele = $( "#" + id )[0];
                $( "#" + id ).bind( "click", function() {
                    funct(ele);
                });
            }
        },
        createButton : function(itemImg, tooltip) {
            var inner = document.createElement('div');
            var elt = document.createElement('a');
            elt.setAttribute('href', 'javascript:void(0);');
            elt.className = 'toolButton';
            inner.className = 'toolbtn-' + this.replaceAll(tooltip, ' ', '_');
            inner.setAttribute('style', 'width: 19px; height: 19px;');
            elt.appendChild(inner);
            return elt;
        },
        createCSS : function(itemImg, tooltip) {
            var html = this.btnCSS.innerHTML;
            var append = '.toolbtn-' + this.replaceAll(tooltip, ' ', '_') + '{ background-image: url(' + itemImg + '); background-size: 19px 19px; }';
            this.btnCSS.innerHTML = html + append;
        },
        createLabel : function(label) {
            var elt = document.createElement('a');
            elt.setAttribute('href', 'javascript:void(0);');
            elt.className = 'toolLabel';
            mxUtils.write(elt, label);
            return elt;
        },
        replaceAll : function(targetStr, strFind, strReplace) {
            var index = 0;
            while (targetStr.indexOf(strFind, index) != -1) {
                targetStr = targetStr.replace(strFind, strReplace);
                index = targetStr.indexOf(strFind, index);
            }
            return targetStr;
        }
        
    });
