/*
 * SpreadJS utility wrapper 
 *
 * author: Yung Long Li
 */
    var SpreadUtil = $Class.extend({
        classProp : { name : "SpreadUtil"},
        sheetIdx : 1,
        keyColumnIdx : null,
        containerId : null,
        spread : null,
        spread_sheet_index: null,
        origionalCopyAction : null,
        origionalPasteAction : null,
        origionalRedoAction : null,
        origionalUndoAction : null,
        cutCopyRange : null,
        copyData : null,
        colorStyleMap : null,
        headerNameMap : null,
        zoomFactor : 1,
        spreadNS : null,
        events : ['ActiveSheetChanged', 'ActiveSheetChanging', 'ButtonClicked', 'CellChanged', 'CellClick', 'CellDoubleClick', 'ClipboardChanged', 'ClipboardChanging', 'ClipboardPasted', 'ClipboardPasting',
                  'ColumnChanged', 'ColumnWidthChanged', 'ColumnWidthChanging', 'CommentChanged', 'CommentRemoved', 'CommentRemoving', 'CultureChanged', 'CustomFloatingObjectLoaded', 'DragDropBlock', 'DragDropBlockCompleted',
                  'DragFillBlock', 'DragFillBlockCompleted', 'EditChange', 'EditEnd', 'EditEnded', 'EditEnding', 'EditStarted', 'EditStarting', 'EditorStatusChanged', 'EnterCell', 'FloatingObjectChanged', 'FloatingObjectRemoved',
                  'FloatingObjectRemoving', 'FloatingObjectSelectionChanged', 'InvalidOperation', 'LeaveCell', 'LeftColumnChanged', 'PictureChanged', 'PictureSelectionChanged', 'RangeChanged', 'RangeFiltered', 'RangeFiltering',
                  'RangeGroupStateChanged', 'RangeGroupStateChanging', 'RangeSorted', 'RangeSorting', 'RowChanged', 'RowHeightChanged', 'RowHeightChanging', 'SelectionChanged', 'SelectionChanging', 'SheetNameChanged', 'SheetNameChanging',
                  'SheetTabClick', 'SheetTabDoubleClick', 'SlicerChanged', 'SparklineChanged', 'TableFiltered', 'TableFiltering' , 'TopRowChanged', 'TouchToolStripOpening', 'UserFormulaEntered', 'UserZooming', 'ValidationError', 'ValueChanged'
                  ],
        paintSuspended : false,
        init: function(div){
            this.containerId = div;
            this.origionalCopyAction = this.spreadNS.SpreadActions.copy;
            this.origionalPasteAction = this.spreadNS.SpreadActions.paste;
            this.origionalRedoAction = this.spreadNS.SpreadActions.redo;
            this.origionalUndoAction = this.spreadNS.SpreadActions.undo;
            this.spread_sheet_index = {};
            this.spread_sheet_index[0] = 'Sheet1';
            this.colorStyleMap = {};
            this.headerNameMap = {};
            return this;
        },
        suspendPaint : function () {
            if (this.paintSuspended == false) {
                var sheet = this.getActiveSheet();
                this.paintSuspended = true;
                sheet.isPaintSuspended(true);
            }
        },
        resumePaint : function() {
            if (this.paintSuspended == true) {
                var sheet = this.getActiveSheet();
                this.paintSuspended = false;
                sheet.isPaintSuspended(false);
                sheet.repaint();
            }
        },
        addSheet : function (name) {
            var sheetCount = this.spread.getSheetCount();
            this.spread.addSheet(sheetCount);
            sheetCount = this.spread.getSheetCount();
            var sheet = this.spread.getSheet(sheetCount - 1);
            sheet.setName(name);
            this.spread_sheet_index[this.sheetIdx] = name;
        },
        setActiveSheet : function (id) {
            this.spread.setActiveSheet(this.spread_sheet_index[id]);
        },
        getSheet : function (name) {
            if (name != null) {
                return this.spread.getSheet(this.spread_sheet_index[name]);    
            } else {
                return null;
            }
        },
        setKeyColumnIdx : function (keyColumnIdx) {
            this.keyColumnIdx = keyColumnIdx;
        },
        setJsonData : function (headerJson, contentJson){
            var header = [];
            var contents = [];
            if (headerJson != null && headerJson != '') header = jQuery.parseJSON(headerJson);
            if (contentJson != null && contentJson != '') contents = jQuery.parseJSON(contentJson);
            this.setExcelData(header, contents);
        },
        setExcelData : function (header, contents){
            var sheet = this.getActiveSheet();
            this.setColumnWidth(header.length + 1);
            this.setRowNumber(contents.length + 2);
            if (this.paintSuspended == false) sheet.isPaintSuspended(true);
            if (header != null && header.length > 0) {
                for ( var i = 0; i < header.length; i++) {
                    sheet.setValue(0, i, header[i].headTitle);
                    this.headerNameMap[i + ''] = header[i].headName;
                }
            }
            if (contents != null && contents != '' && contents.length > 0) {
                var rowIdx = 1;
                for ( var i = 0; i < contents.length; i++) {
                    var rowData = contents[i];
                    for ( var j = 0; j < rowData.length; j++) {
                        sheet.setValue(rowIdx, j, rowData[j]);
                    }
                    rowIdx++;
                }
            }
            if (header != null && header.length > 0) {
                for ( var i = 0; i < header.length; i++) {
                    if (header[i].validateValues != null && header[i].validateValues != '') {
                        this.setValidation(header[i].columnIdx, header[i].validateValues);
                    }
                    if (header[i].rgbColor != null && header[i].rgbColor != '' && header[i].rgbColor != 'rgb(255, 255, 255)') {
                        this.setColumnColor(header[i].columnIdx, header[i].rgbColor);
                    }
                }
            }
            this.spread.highlightInvalidData(true);
            if (this.paintSuspended == false) sheet.isPaintSuspended(false);
        },
        setColumnWidth : function (headerLength) {
            var sheet = this.getActiveSheet();
            sheet.setColumnCount(headerLength, this.spreadNS.SheetArea.viewport);
            for ( var i = 0; i < headerLength; i++) {
                sheet.setColumnWidth(i, 100);
            }
        },
        setRowNumber : function (contentLength) {
            var sheet = this.getActiveSheet();
            sheet.setRowCount(contentLength, this.spreadNS.SheetArea.viewport);
        },
        getActiveSheet : function() {
            return this.spread.getActiveSheet();
        },
        frozen : function(x, y) {
            var sheet = this.getActiveSheet();
            if (x != null && y != null) {
                sheet.setFrozenCount(x, y);
                return;
            }
            var activeRowIndex = sheet.getActiveRowIndex();
            var activeColumnIndex = sheet.getActiveColumnIndex();
            sheet.setFrozenCount(activeRowIndex, activeColumnIndex);
        },
        paste : function() {
            var sheet = this.getActiveSheet();
            sheet.isPaintSuspended(true);
            /*
            if (this.cutCopyRange) {
                var pasteRanges = sheet.getSelections();
                var sheetRowCount = sheet.getRowCount();
                var sheetColumnCount = sheet.getColumnCount();
                var pasteRange;
                for (var i = 0; i < pasteRanges.length; i++) {
                    pasteRange = pasteRanges[i];
                    if (pasteRange.row + this.cutCopyRange.rowCount > sheetRowCount) {
                        sheet.setRowCount(pasteRange.row + this.cutCopyRange.rowCount);
                        sheetRowCount = pasteRange.row + this.cutCopyRange.rowCount;
                    }
                    if (pasteRange.col + this.cutCopyRange.colCount > sheetColumnCount) {
                        sheet.setColumnCount(pasteRange.col + this.cutCopyRange.colCount);
                        sheetColumnCount = pasteRange.col + this.cutCopyRange.colCount;
                    }
                }
            }
            */
            this.origionalPasteAction.call(sheet);
            sheet.isPaintSuspended(false);
        },
        addRow : function(rows) {
            var sheet = this.getActiveSheet();
            var rowCnt = sheet.getRowCount();
            if (rows == null) {
                sheet.setRowCount(rowCnt + 1, this.spreadNS.SheetArea.viewport);
            } else {
                sheet.setRowCount(rowCnt + rows, this.spreadNS.SheetArea.viewport);
            }
        },
        hideColumn : function(column) {
            var sheet = this.getActiveSheet();
            var activeColumnIndex = sheet.getActiveColumnIndex();
            if (column != null) {
                activeColumnIndex = column;
            }
            sheet.setColumnVisible(activeColumnIndex,false);
        },
        copy : function() {
            var sheet = this.getActiveSheet();
            //this.cutCopyRange = sheet.getSelections()[0];
            this.origionalCopyAction.call(sheet);
        },
        redo : function() {
            var sheet = this.getActiveSheet();
            this.origionalRedoAction.call(sheet);
        },
        undo : function() {
            var sheet = this.getActiveSheet();
            this.origionalUndoAction.call(sheet);
        },
        setCopyData : function(data){
            this.copyData = data;
        },
        toJsonStr : function () {
            var jsonString = JSON.stringify(this.spread.toJSON());
            return jsonString;
        },
        fromJsonStr : function (jsonStr) {
            this.spread.fromJSON(JSON.parse(jsonStr));
        },
        setFilter : function() {
            var sheet = this.getActiveSheet();
            var filter = sheet.rowFilter();
            if (filter == null) {
                this.enableFilter();
            } else {
                this.disableFilter();
            }
        },
        enableFilter : function () {
            var sheet = this.getActiveSheet();
            var headerLength = Object.keys(this.headerNameMap).length;
            if (headerLength == 0) {
                headerLength = this.findHeaderLength();
            }
            var filter = new this.spreadNS.HideRowFilter(new this.spreadNS.Range(1, 0, sheet.getRowCount() - 1, headerLength));
            sheet.rowFilter(filter);
            filter.setShowFilterButton(true);
        },
        findHeaderLength : function() {
            var sheet = this.getActiveSheet();
            for (var i = sheet.getColumnCount() - 1; i > 0; i--) {
                var value = sheet.getValue(0, i);
                if (value != null) {
                    return i + 1;
                }
            }
            return 0;
        },
        disableFilter : function () {
            var sheet = this.getActiveSheet();
            sheet.rowFilter(null);
        },
        getDataType : function () {
            var sheet = this.getActiveSheet();
            var result = [];
            var rowCnt = sheet.getRowCount();
            var headerLength = Object.size(this.headerNameMap);
            for (var i = 0; i <= headerLength; i++) {
                var rowIdx = 0;
                var found = false;
                while (rowIdx < rowCnt) {
                    var value = sheet.getValue(rowIdx, i);
                    if (value != null) {
                        var dataType = typeof value;
                        if (dataType == 'object') {
                            if (typeof value.getMonth === 'function') {
                                result.push('date');
                            } else {
                                result.push('undefined');
                            }
                        } else {
                            result.push(dataType);
                        }
                        found = true;
                        break;
                    } else {
                        rowIdx++;
                    }
                }
                if (!found) {
                    result.push(null);
                }
            }
            return result;
        },
        getDataRowLength : function(keyColumns) {
            var sheet = this.getActiveSheet();
            if (keyColumns == null) {
                keyColumns = [];
                var columnCnt = sheet.getColumnCount();
                for (var i = 0; i < columnCnt; i++) {
                    keyColumns.push(i);
                }
            }
            var rowCnt = sheet.getRowCount();
            var rowIdx = 0;
            while (rowIdx < rowCnt) {
                var empty = true;
                for (var i = 0; i < keyColumns.length; i++) {
                    var value = sheet.getValue(rowIdx, keyColumns[i]);
                    if (value != null) {
                        empty = false;
                        break;
                    }
                }
                if (empty) {
                    return rowIdx;
                }
                rowIdx++;
            }
            return rowCnt;
        },
        setFormat : function(formatStr, col, row) {
            var sheet = this.getActiveSheet();
            if (col == null || row == null) {
                row = sheet.getActiveRowIndex();
                col = sheet.getActiveColumnIndex();
            }
            sheet.getCell(row, col).formatter(formatStr);
        },
        getColorStyle : function (rgb) {
            if (rgb == null) return null; 
            var colorStyle = new this.spreadNS.Style();
            colorStyle.backColor = rgb;
            return colorStyle;
        },
        setCellColor : function(rowIdx, colIdx, rgb){
            var sheet = this.getActiveSheet();
            var cell = sheet.getCell(rowIdx, colIdx);
            var cellStyle = sheet.getStyle(rowIdx, colIdx);
            if (cellStyle != null) {
                if (rgb == null) {
                    if (cellStyle.backColor != null) {
                        delete cellStyle.backColor;
                        sheet.setStyle(rowIdx, colIdx, cellStyle, this.spreadNS.SheetArea.viewport);
                    }
                } else {
                    cell.backColor(rgb);
                }
            } else {
                if (rgb != null) cell.backColor(rgb); 
            }
        },
        setRowColor : function(rowIdx, rgb){
            var sheet = this.getActiveSheet();
            var rowStyle = sheet.getStyle(rowIdx, -1);
            if (rowStyle != null) {
                if (rgb == null) {
                    if (rowStyle.backColor != null) {
                        delete rowStyle.backColor;
                        sheet.setStyle(rowIdx, -1, rowStyle, this.spreadNS.SheetArea.viewport);
                    }
                } else {
                    rowStyle.backColor = rgb;
                    sheet.setStyle(rowIdx, -1, rowStyle, this.spreadNS.SheetArea.viewport);
                }
            } else {
                rowStyle.backColor = rgb;
                if (rgb != null) sheet.setStyle(rowIdx, -1, rowStyle, this.spreadNS.SheetArea.viewport);
            }
        },
        setColumnColor : function(colIdx, rgb){
            var sheet = this.getActiveSheet();
            var colStyle = sheet.getStyle(-1, colIdx);
            if (colStyle != null) {
                if (rgb == null) {
                    if (colStyle.backColor != null) {
                        delete colStyle.backColor;
                        sheet.setStyle(-1, colIdx, colStyle, this.spreadNS.SheetArea.viewport);
                    }
                } else {
                    colStyle.backColor = rgb;
                    sheet.setStyle(-1, colIdx, colStyle, this.spreadNS.SheetArea.viewport);
                }
            } else {
                colStyle.backColor = rgb;
                if (rgb != null) sheet.setStyle(-1, colIdx, colStyle, this.spreadNS.SheetArea.viewport);
            }
        },
        setColor : function (colorCode) {
            var sheet = this.getActiveSheet();
            var range = sheet.getSelections()[0];
            if (range.row == -1) {
                this.setColumnColor(range.col, colorCode);
                return;
            }
            if (range.col == -1) {
                this.setRowColor(range.row, colorCode);
                return;
            }
            if (this.paintSuspended == false) sheet.isPaintSuspended(true);
            for (var r = range.row; r < (range.row + range.rowCount); r++) {
                for (var c = range.col; c < (range.col + range.colCount); c++) {
                    this.setCellColor(r, c, colorCode);
                }
            }
            var colorCode = $('#spreadjs-colorInput').val('');
            if (this.paintSuspended == false) sheet.isPaintSuspended(false);
        },
        setBasicValidation : function(colIdx, values) {
            var sheet = this.getActiveSheet();
            var dv1 = new this.spreadNS.DefaultDataValidator.createListValidator(values);
            var rowCnt = this.getDataRowLength();
            for (var i = 1; i < rowCnt; i++) {
                sheet.setDataValidator(i, colIdx, dv1);
            }
        },
        getObjectArray : function(header, colIdxs) {
            var sheet = this.getActiveSheet();
            if (header == null) {
                header = [];
                colIdxs = [];
                var columnCnt = sheet.getColumnCount();
                for (var i = 0; i < columnCnt; i++) {
                    var value = sheet.getValue(0, i);
                    if (value == null) {
                        header.push(i + '');
                    } else {
                        header.push(value + '');
                    }
                    colIdxs.push(i);
                }
            }
            var length = this.getDataRowLength();
            var colLength = header.length;
            var objectArray = [];
            for (var r = 1; r < length; r++) {
                var obj = {};
                for (var c = 0; c < colLength; c++) {
                    var value = sheet.getValue(r, colIdxs[c]);
                    if (value != null) obj[header[c]] = value;
                }
                objectArray.push(obj);
            }
            return objectArray;
        },
        zoomIn : function(){
            var sheet = this.getActiveSheet();
            if (this.zoomFactor >= 1 && this.zoomFactor <= 4) {
                this.zoomFactor = this.zoomFactor + 0.5;
                if (this.zoomFactor > 4) this.zoomFactor = 4;
            } else if (this.zoomFactor < 1){
                this.zoomFactor = this.zoomFactor * 2;
            }
            sheet.zoom(this.zoomFactor);
        },
        zoomOut : function(){
            var sheet = this.getActiveSheet();
            if (this.zoomFactor >= 1 && this.zoomFactor <= 4) {
                this.zoomFactor = this.zoomFactor - 0.5;
            } else if (this.zoomFactor < 1){
                this.zoomFactor = this.zoomFactor / 2;
                if (this.zoomFactor < 0.25) this.zoomFactor = 0.25;
            }
            sheet.zoom(this.zoomFactor);
        },
        setBorder : function(style) {
            var sheet = this.getActiveSheet();
            var range = sheet.getSelections()[0];
            var lineStyle = this.spreadNS.LineStyle.medium;
            if (style != null) {
                if (style == 'empty') {
                    lineStyle = this.spreadNS.LineStyle.empty;
                } else if (style == 'medium') {
                    lineStyle = this.spreadNS.LineStyle.medium;
                } else if (style == 'thin') {
                    lineStyle = this.spreadNS.LineStyle.thin;
                } else if (style == 'thick') {
                    lineStyle = this.spreadNS.LineStyle.thick;
                } else if (style == 'dashed') {
                    lineStyle = this.spreadNS.LineStyle.dashed;
                } else if (style == 'dotted') {
                    lineStyle = this.spreadNS.LineStyle.dotted;
                } else if (style == 'double') {
                    lineStyle = this.spreadNS.LineStyle.double;
                }
            }
            var lineBorder = new this.spreadNS.LineBorder('black', lineStyle);
            var sheetArea = this.spreadNS.SheetArea.viewport
            sheet.setBorder(range, lineBorder, { left: true, right: true, top: true, bottom: true}, sheetArea);
        },
        setComment : function(text) {
            var sheet = this.getActiveSheet();
            var comment = new this.spreadNS.Comment();
            comment.text(text);
            var activeRowIndex = sheet.getActiveRowIndex();
            var activeColumnIndex = sheet.getActiveColumnIndex();
            sheet.setComment(activeRowIndex, activeColumnIndex, comment);
        },
        removeComment : function() {
            var sheet = this.getActiveSheet();
            var activeRowIndex = sheet.getActiveRowIndex();
            var activeColumnIndex = sheet.getActiveColumnIndex();
            sheet.setComment(activeRowIndex, activeColumnIndex, '');
            sheet.repaint();
        },
        showComment : function(){
            var sheet = this.getActiveSheet();
            var activeRowIndex = sheet.getActiveRowIndex();
            var activeColumnIndex = sheet.getActiveColumnIndex();
            var comment = sheet.getComment(activeRowIndex, activeColumnIndex);
            if (comment != null) {
                var mode = comment.displayMode();
                if (mode == this.spreadNS.DisplayMode.HoverShown) {
                    comment.displayMode(this.spreadNS.DisplayMode.AlwaysShown);
                } else {
                    comment.displayMode(this.spreadNS.DisplayMode.HoverShown);
                }
            }
        },
        sheetBind : function(returnEventbus) {
            var sheet = this.getActiveSheet();
            for (var i = 0; i < this.events; i++) {
                var evt = spreadNS.Events[this.events[i]];
                sheet.bind(evt, function(e, args) {
                    returnEventbus[this.events[i]](e, args);
                });
            }
        }
    });
    